# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0


import subprocess

import click


@click.command()
def check():
    # print(__file__)
    # print(os.getcwd())
    # print(os.environ["PATH"])
    subprocess.run(["which", "pytest"])
    subprocess.run(
        [
            "pytest",
            "-o",
            "console_output_style=count",
            "--disable-warnings",
            "--no-header",
            "--no-showlocals",
            "--show-capture=no",
            # "--tb=line",
            # "--quiet",
            # "--no-summary",
            "--verbose",
        ]
    )
