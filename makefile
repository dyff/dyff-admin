# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

VENV ?= venv
PYTHON ?= $(VENV)/bin/python3
PIP ?= $(PYTHON) -m pip
IMAGE ?= dyff

BASE_DIR = $(shell pwd)
PYTHONPATH = $(BASE_DIR)

.PHONY: all
all: setup

.PHONY: setup
setup: $(VENV)/requirements.txt

$(VENV):
	uv venv $(VENV)

requirements.txt: pyproject.toml | $(VENV)
	uv pip compile -o "$@" "$<"

$(VENV)/requirements.txt: requirements.txt | $(VENV)
	VIRTUAL_ENV=$(VENV) uv pip install -r "$<"

.PHONY: clean
clean:
	find -name __pycache__ -type d -exec rm -rf '{}' \;
	find -name \*.pyc -type f -exec rm -f '{}' \;

.PHONY: distclean
distclean:
	rm -rf node_modules/ $(VENV)
