# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

[build-system]
requires = [
    "setuptools>=60",
    "setuptools-scm>=8.0",
    "wheel",
]
build-backend = "setuptools.build_meta"

[project]
name = "dyff-admin"
description = "Admin interface for the Dyff AI auditing platform."

authors = [
    {name = "Digital Safety Research Institute", email = "contact@dsri.org"},
]

classifiers = [
    "Development Status :: 3 - Alpha",
    "Intended Audience :: Science/Research",
    "License :: OSI Approved :: Apache Software License",
    "Operating System :: OS Independent",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3.9",
    "Programming Language :: Python :: 3.10",
    "Programming Language :: Python :: 3.11",
    "Programming Language :: Python :: 3.12",
    "Topic :: Scientific/Engineering :: Artificial Intelligence",
]

dependencies = [
    "click",
    "dyff-audit",
    "pydantic",
    "pytest",
    "pytest-datafiles",
    "pytest-depends",
]

dynamic = ["version"]

keywords = [
    "ai",
    "audit",
    "safety",
    "evaluation",
]

license = {text = "Apache-2.0"}

readme = "README.md"

requires-python = ">=3.9"

[project.scripts]
dyff-admin = "dyff.admin.__main__:main"

[project.urls]
Home = "https://gitlab.com/dyff/dyff-admin"
Issues = "https://gitlab.com/dyff/dyff-admin/-/issues"

[tool.setuptools.packages.find]
where = ["."]
include = [
    "dyff.admin*",
]
namespaces = true

[tool.setuptools_scm]

[tool.deptry]
exclude = ["dyff/admin/tests/data", "venv", "build"]
known_first_party = ["dyff"]

[tool.deptry.package_module_name_map]
dyff-audit = "dyff"

[tool.deptry.per_rule_ignores]
DEP002 = ["pytest-datafiles", "pytest-depends", "dyff-audit", "pydantic", "pytest"]

[tool.importlinter]
root_packages = [
    "dyff.admin",
]
include_external_packages = true

[tool.isort]
known_first_party = ["dyff"]
profile = "black"
